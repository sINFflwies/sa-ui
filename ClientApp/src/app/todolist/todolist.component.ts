import { Component, OnInit } from '@angular/core';
import {Todo, TodoService} from '../todo.service';
import {take} from 'rxjs/operators';
import { AuthService } from '../auth.service';
import { TodoFormService } from './todo-form.service';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.scss']
})
export class TodolistComponent implements OnInit {

  constructor(
    public todoService: TodoService,
    public authService: AuthService,
    public todoFormService: TodoFormService
  ) { }

  public ngOnInit(): void {
    this.authService.userChange.subscribe(() => {
      if (this.authService.isLoggedIn) {
        this.todoService.getTodos().pipe(take(1)).subscribe();
      }
    });
    
  }

  public createTodo(): void {
    this.todoService.createTodo({toDo: this.todoFormService.text.value} as Todo).pipe(take(1)).subscribe(
      () => {
        this.todoFormService.text.setValue(null);
      }
    );
  }
  
  public updateTodo(todo: Todo): void {
    todo.isDone = !todo.isDone;
    this.todoService.updateTodo(todo).pipe(take(1)).subscribe(
      () => {
        this.todoFormService.text.setValue(null);
      }
    );
  }

  public deleteTodo(todo: Todo): void {
    this.todoService.deleteTodo(todo).pipe(take(1)).subscribe();
  }

}
