import { Component, OnInit } from '@angular/core';
import { RegisterUiService } from './register-ui.service';
import { AuthService } from '../../auth.service';
import { take } from 'rxjs/operators';
import { DialogRole, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-register-dialog',
  templateUrl: './register-dialog.component.html',
  styleUrls: ['./register-dialog.component.scss']
})
export class RegisterDialogComponent implements OnInit {

  constructor(
    public registerUiService: RegisterUiService,
    private authService: AuthService,
    private dialogRef: MatDialogRef<RegisterDialogComponent>
  ) { }

  ngOnInit() {
  }

  public login(): void {
    if (this.registerUiService.fields.valid) {
      this.authService.login(this.registerUiService.email.value, this.registerUiService.password.value)
        .pipe(take(1))
        .subscribe(
          () => {
            this.dialogRef.close();
          },
          (error) => {
            alert('Es ist ein Fehler aufgetreten.');
            console.error(error);
          }
        );
    } else {
      alert('Das Formular enthaelt noch Fehler');
    }
  }

  public register(): void {
    console.log('register');
    if (this.registerUiService.fields.valid) {
      this.authService.register(this.registerUiService.email.value, this.registerUiService.password.value)
      .pipe(take(1))
      .subscribe(
        () => {
          this.dialogRef.close();
        },
        (error) => {
          alert('Es ist ein Fehler aufgetreten.');
          console.error(error);
        }
      );
    } else {
      alert('Das Formular enthaelt noch Fehler');
    }
  }

}
