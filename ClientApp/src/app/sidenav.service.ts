import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidenavService {

  /** Determines if the sidenav is opened or not. Is binded to the html. */
  public opened: boolean;

  constructor() {
    this.opened = false;
  }

  /** Toggles the sidenav. */
  public toggle(): void {
    this.opened = !this.opened;
  }
}
