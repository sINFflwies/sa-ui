import { TestBed } from '@angular/core/testing';

import { RegisterUiService } from './register-ui.service';

describe('RegisterUiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegisterUiService = TestBed.get(RegisterUiService);
    expect(service).toBeTruthy();
  });
});
