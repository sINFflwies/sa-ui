import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { RegisterDialogComponent } from '../auth/register-dialog/register-dialog.component';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-user-button',
  templateUrl: './user-button.component.html',
  styleUrls: ['./user-button.component.scss']
})
export class UserButtonComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    public authService: AuthService
  ) { }

  ngOnInit() {
  }

  public login(): void {
    this.dialog.open(RegisterDialogComponent, {maxWidth: '400px', maxHeight: '500px'});
  }

  public logout(): void {
    this.authService.logout();
  }

}
