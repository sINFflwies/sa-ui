import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class TodoFormService {

  public readonly text: FormControl = new FormControl(null, Validators.required);

  public readonly fields: FormGroup = new FormGroup({
    toDo: this.text
  });


  constructor() { }
}
