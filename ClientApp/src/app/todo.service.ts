import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators';
import * as _ from 'underscore';

export interface Todo {
  isDone: boolean;
  userId: number;
  toDo: string;
  id: string;
  deadlineDate: Date;
}

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  public todos: Todo[] = [];

  constructor(
    private http: HttpClient
  ) { }

  public getTodos(): Observable<Todo[]> {
    return this.http.get<Todo[]>(this.todosUrl)
      .pipe(tap((todos: Todo[]) => this.todos = todos));
  }

  public createTodo(todo: Todo): Observable<Todo> {
    todo.deadlineDate = new Date();
    return this.http.post<Todo>(this.todosUrl, todo)
      .pipe(tap((t: Todo) => this.todos.push(t)));
  }

  public updateTodo(todo: Todo): Observable<Todo> {
    return this.http.put<Todo>(`${this.todosUrl}/${todo.id}`, todo)
      .pipe(tap((t: Todo) => {
        for (let i = 0; i < this.todos.length; i++) {
          if (this.todos[i].id === t.id) {
            this.todos[i] = t;
          }
        }
      }));
  }

  public deleteTodo(todo): Observable<void> {
    return this.http.delete<void>(`${this.todosUrl}/${todo.id}`)
      .pipe(tap(() => {
        this.todos = _.reject(this.todos, (t) => t.id === todo.id);
      }));
  }

  get todosUrl(): string {
    return `${environment.todoServiceUrl}`;
  }

}
