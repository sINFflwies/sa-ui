import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatIconModule, MatListModule, MatSidenavModule, MatToolbarModule, MatDialogModule, MatTabsModule, MatInputModule, MatFormFieldModule, MatExpansionPanelDescription, MatExpansionModule} from '@angular/material';
import { TodolistComponent } from './todolist/todolist.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { UserButtonComponent } from './user-button/user-button.component';
import { HTTP_INTERCEPTORS, HttpInterceptor, HttpClientModule } from '@angular/common/http';
import { HttpInterceptorService } from './http-interceptor.service';
import { RegisterDialogComponent } from './auth/register-dialog/register-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    TodolistComponent,
    SidenavComponent,
    UserButtonComponent,
    RegisterDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatCheckboxModule,
    MatDialogModule,
    MatTabsModule,
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatExpansionModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [RegisterDialogComponent]
})
export class AppModule { }
