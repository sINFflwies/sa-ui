import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class RegisterUiService {

  public readonly email: FormControl = new FormControl(null, Validators.email);
  public readonly password: FormControl = new FormControl(null, null);

  public readonly fields: FormGroup = new FormGroup({
    email: this.email,
    password: this.password
  });



  constructor() { }
}
