

export interface IUser {
    email: string;
    id: number;
    token: string;
}
