export interface IEnvironment {
    production: boolean;
    idpUrl: string;
    todoServiceUrl: string;
}
