import { TestBed } from '@angular/core/testing';

import { TodoFormService } from './todo-form.service';

describe('TodoFormService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TodoFormService = TestBed.get(TodoFormService);
    expect(service).toBeTruthy();
  });
});
