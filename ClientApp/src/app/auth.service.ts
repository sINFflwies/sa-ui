import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IUser } from './i-user';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public userChange: Subject<IUser> = new Subject();

  private user: IUser = null;
  

  constructor(
    private http: HttpClient
  ) { }


  public getJwtToken(): string {
    if (this.user) {
      return this.user.token;
    }
    return null;
  }

  public getEmail(): string {
    if (this.user) {
      return this.user.email;
    }
    return null;
  }

  public login(email: string, password: string): Observable<IUser> {
    return this.http.post<IUser>(this.loginUrl, {email, password})
      .pipe(
        tap((user: IUser) => {
          this.user = user;
          this.userChange.next(this.user);
        })
      );
  }

  public register(email: string, password: string): Observable<IUser> {
    return this.http.post<IUser>(this.registerUrl, {email, password})
      .pipe(
        tap((user: IUser) => {
          this.user = user;
          this.userChange.next(this.user);
        })
      );
  }

  public logout(): void {
    this.user = null;
    this.userChange.next(null);
  }

  get loginUrl(): string {
    return `${environment.idpUrl}/Auth/Login`;
  }

  get registerUrl(): string {
    return `${environment.idpUrl}/Auth/Register`;
  }

  get isLoggedIn(): boolean {
    return this.user !== null;
  }

}
